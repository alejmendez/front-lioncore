const trans = {
    'sliders' : [
        {
            title: 'Perfect Balance',
            detail: 'ArchitectUI is like a dream. Some think it\'s too good to be true! Extensive collection of unified Vue Bootstrap Components and Elements.'
        },
        {
            title: 'Scalable, Modular, Consistent',
            detail: 'Easily exclude the components you don\'t require. Lightweight, consistent Bootstrap based styles across all elements and components'
        },
        {
            title: 'Complex, but lightweight',
            detail: 'We\'ve included a lot of components that cover almost all use cases for any type of application.'
        }
    ]
}

export default trans