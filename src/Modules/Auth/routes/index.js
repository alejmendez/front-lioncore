const metaRoute = { layout: 'userpages', auth: false }
const routes = [
    {
        path: '/login',
        name: 'login',
        meta: metaRoute,
        component: () => import('../views/Login.vue'),
    },
    {
        path: '/pages/login-boxed',
        name: 'login-boxed',
        meta: metaRoute,
        component: () => import('../views/LoginBoxed.vue'),
    },
    {
        path: '/pages/register',
        name: 'register',
        meta: metaRoute,
        component: () => import('../views/Register.vue'),
    },
    {
        path: '/pages/register-boxed',
        name: 'register-boxed',
        meta: metaRoute,
        component: () => import('../views/RegisterBoxed.vue'),
    },
    {
        path: '/pages/forgot-password',
        name: 'forgot-password',
        meta: metaRoute,
        component: () => import('../views/ForgotPassword.vue'),
    },
    {
        path: '/pages/forgot-password-boxed',
        name: 'forgot-password-boxed',
        meta: metaRoute,
        component: () => import('../views/ForgotPasswordBoxed.vue'),
    },
]

export default routes;